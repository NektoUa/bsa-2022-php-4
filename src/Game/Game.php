<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Commands\Dispatcher;
use BinaryStudioAcademy\Game\Galaxy\Galaxy;

class Game
{
    private $random;
    private $currentGalaxy;
    private $dispatcher;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->currentGalaxy = new Galaxy();
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Your goal is to develop a game "Galaxy Warriors".');
        $writer->writeln('Here you need to realize infinite loop with game logic.');
        $writer->writeln('Use proposed implementation in order to tests work correct.');
        $writer->writeln('Random float number: ' . $this->random->get());
        $this->dispatcher = new Dispatcher($writer);
        $writer->writeln('Adventure has begun. Wish you good luck!');
        while (true) {
            $writer->writeln('Press enter to start... ');
            $input = trim($reader->read());
            $this->dispatcher->go($input, $this->currentGalaxy, $this->random);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
    }
}
