<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Starship;

class EnemySpaceShip implements
    \BinaryStudioAcademy\Game\Contracts\Builder
{
    private $classShip;
    private $ship;

    public function __construct(string $classShip)
    {
        $this->classShip = $classShip;
    }

    public function createShip()
    {
        $this->ship = new Spaceships($this->classShip);
    }

    public function setClass(): void
    {
        $this->ship->classShip = $this->classShip;
    }

    public function getShip(): Starship
    {
        print_r($this->ship);
        return $this->ship;
    }
}
