<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Builder;

class Director
{
    public function build(Builder $builder)
    {
        $builder->createShip();
        $builder->setClass();
        return $builder->getShip();
    }
}
