<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Starship;
use BinaryStudioAcademy\Game\Ships\EnemySpaceShip;

class CreateSpaceship
{
    private $classOfShip;
    private $random;

    public function __construct(string $classOfShip, $random)
    {
        $this->classOfShip = $classOfShip;
        $this->random = $random;
    }

    public function createShip(): Starship
    {
        $shipOfStar = new EnemySpaceShip($this->classOfShip);
        return (new Director())->build($shipOfStar);
    }
}
