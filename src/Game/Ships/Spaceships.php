<?php

namespace BinaryStudioAcademy\Game\Ships;

class Spaceships
{
    public const SPACESHIPS = [
        'patrol' => [
            'name' => 'Patrol Spaceship',
            'stats' => [
                'strength' => 4,
                'armor' => 4,
                'luck' => 2,
                'health' => 100,
            ]
        ],
        'battle' => [
            'name' => 'Battle Spaceship',
            'stats' => [
                'strength' => 8,
                'armor' => 7,
                'luck' => 6,
                'health' => 100,
            ]
        ],
        'executor' => [
            'name' => 'Executor',
            'stats' => [
                'strength' => 10,
                'armor' => 10,
                'luck' => 10,
                'health' => 100,
            ]
        ]
    ];

    public function __construct(string $classShip)
    {
        $this->classShip = $classShip;
    }

    public function getShip()
    {
        return self::SPACESHIPS[$this->classShip]['name'];
    }
}
