<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\CreateSpaceship;

class Galaxy extends Galaxies
{
    public $galaxyName;
    public $galaxyEnemy;

    public function __construct()
    {
        $this->galaxyName = 'home';
    }

    public function showName(): string
    {
        return self::GALAXIES[$this->galaxyName]['galaxy'];
    }

    public function showEnemyShip($random)
    {
        $enemyShip = new CreateSpaceship(self::GALAXIES[$this->galaxyName]['spaceship'], $random);
        $this->galaxyEnemy = $enemyShip->createShip();
    }
}
