<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Builder
{
    public function createShip();
    public function setClass(): void;
    public function getShip(): Starship;
}
