<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Starship
{
    public function getShip(): string;
}
