<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Command
{
    public function execute(): void;
}
