<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface CreateStarship
{
    public function createShip(): Starship;
}
