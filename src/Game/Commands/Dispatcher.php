<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Commands\Invoker;
use BinaryStudioAcademy\Game\Galaxy\Galaxy;
use PHPUnit\TextUI\Command;

class Dispatcher
{
    private $writer;
    private $invoker;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
        $this->invoker = new Invoker();
    }

    public function go($command_arr, Galaxy $galaxy, Random $random)
    {
        $arr = explode(' ', $command_arr);
        if (count($arr) <= 1) {
            $command = $arr[0];
            $param = '';
        } else {
            $command = $arr[0];
            $param = $arr[1];
        }
        switch ($command) {
            case 'help':
                $this->invoker->setCommand(new CommandHelp($this->writer));
                $this->invoker->run();
                break;

            case 'whereami':
                $this->invoker->setCommand(new CommandWhereami($this->writer, $galaxy));
                $this->invoker->run();
                break;

            case 'set-galaxy':
                $this->invoker->setCommand(new CommandSetGalaxy($param, $this->writer, $galaxy, $random));
                $this->invoker->run();
                break;

            case 'exit':
                $this->invoker->setCommand(new CommandExit($this->writer));
                $this->invoker->run();
                break;
            default:
                $this->writer->writeln("Sorry. We don't have command '$command'. Please write: 'help'");
        }
    }
}
