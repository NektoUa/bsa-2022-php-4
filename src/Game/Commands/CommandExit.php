<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandExit implements \BinaryStudioAcademy\Game\Contracts\Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $this->writer->writeln('This Is the End !');
        exit;
    }
}
