<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Galaxy\Galaxy;

class CommandWhereami implements \BinaryStudioAcademy\Game\Contracts\Command
{
    private $writer;
    private $galaxy;

    public function __construct(Writer $writer, Galaxy $galaxy)
    {
        $this->writer = $writer;
        $this->galaxy = $galaxy;
    }

    public function execute(): void
    {
        $this->writer->writeln('Galaxy: ' . $this->galaxy->showName());
    }
}
