<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Galaxy\Galaxy;

class CommandSetGalaxy implements \BinaryStudioAcademy\Game\Contracts\Command
{
    private $writer;
    private $galaxy;
    private $target;
    private $random;

    public function __construct(string $target, Writer $writer, Galaxy $galaxy, $random)
    {
        $this->writer = $writer;
        $this->galaxy = $galaxy;
        $this->target = $target;
        $this->random = $random;
    }

    public function execute(): void
    {
        if (array_key_exists($this->target, Galaxy::GALAXIES)) {
            $this->galaxy->galaxyName = $this->target;
            if ($this->target == 'home') {
                $this->writer->writeln('Galaxy: Home.');
                return;
            }
            $this->galaxy->showEnemyShip($this->random);
            $this->writer->writeln('You are in Galaxy: ' . $this->galaxy->showName() . '!');
            $this->writer->writeln('You see an enemy ship: ' . $this->galaxy->galaxyEnemy->getShip() . '!');
        } else {
            $this->writer->writeln("No specified galaxy found.");
        }
    }
}
