<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandHelp implements \BinaryStudioAcademy\Game\Contracts\Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $this->writer->writeln('List of commands:');
        $this->writer->writeln('help - shows this list of commands');
        $this->writer->writeln('stats - shows stats of spaceship');
        $this->writer->writeln('set-galaxy <home|andromeda|spiral|pegasus|shiar|xeno|isop> - provides jump into specified galaxy');
        $this->writer->writeln("attack - attacks enemy's spaceship");
        $this->writer->writeln('grab - grab useful load from the spaceship');
        $this->writer->writeln('buy <strength|armor|reactor> - buys skill or reactor (1 item)');
        $this->writer->writeln('apply-reactor - apply magnet reactor to increase spaceship health level on 20 points');
        $this->writer->writeln('whereami - shows current galaxy');
        $this->writer->writeln('restart - restart game');
        $this->writer->writeln('exit - ends the game');
    }
}
